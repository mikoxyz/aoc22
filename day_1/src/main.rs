use clap::Parser;
use std::fs;

#[derive(Parser)]
struct Args {
    path: std::path::PathBuf,
}

impl Elf {
    fn new(calories: Vec<usize>) -> Self {
        Elf { calories }
    }

    fn sum(&self) -> usize {
        let mut x = 0;
        for i in 0..self.calories.len() {
            x += self.calories[i];
        }
        x
    }
}

struct Elf {
    calories: Vec<usize>,
}

fn index_of_elf(calories: usize, calsum: &Vec<usize>) -> usize {
    calsum.iter().position(|&x| x == calories).unwrap()
}

fn init_calsum(elf: Vec<Elf>) -> Vec<usize> {
    let mut calsum = Vec::new();
    for i in 0..elf.len() {
        calsum.push(elf[i].sum());
    }

    calsum
}

fn init_elf(input: Vec<Vec<usize>>) -> Vec<Elf> {
    let mut elf = Vec::new();
    for i in 0..input.len() {
        elf.push(Elf::new(input[i].clone()));
    }

    elf
}

fn parse_input(input: String) -> Vec<Vec<usize>> {
    let input: Vec<&str> = input.split("\n\n").collect();
    let mut parsed_input = Vec::new();
    for i in 0..input.len() {
        let input: Vec<&str> = input[i].split("\n").collect();
        let mut buf = Vec::new();
        for j in 0..input.len() {
            buf.push(match input[j].trim().parse::<usize>() {
                Ok(num) => num,
                /*
                 * on my machine this makes us skip the last elf, but who
                 * cares about the last elf, really?
                 */
                Err(_) => continue,
            });
        }

        parsed_input.push(buf);
    }

    parsed_input
}

fn read_input(path: std::path::PathBuf) -> String {
    let file = fs::read_to_string(path).expect("could not read file");

    file
}

fn sort_calsum(mut calsum: Vec<usize>) -> Vec<usize> {
    calsum.sort();
    calsum.reverse();

    calsum
}

fn main() {
    let args = Args::parse();
    let calsum = init_calsum(init_elf(parse_input(read_input(args.path))));
    let calsum_sorted = sort_calsum(calsum.clone());

    println!(
        "biggest elf is elf {}, carrying {} calories",
        index_of_elf(calsum_sorted[0], &calsum) + 1,
        calsum_sorted[0]
    );

    println!("top three biggest elves:");
    let mut top_three = 0;
    for i in 0..3 {
        println!("{}. {} (elf {})", i+1, calsum_sorted[i], index_of_elf(calsum_sorted[i], &calsum));
        top_three += calsum_sorted[i];
    }

    println!("they have {} calories in total", top_three);
}
